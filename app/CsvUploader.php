<?php

namespace App;

class CsvUploader
{
    private string $csvName;
    private array $csvData = [];
    private array $people = [];
    private array $fieldSplitIndicators = ['and', '&'];
    private array $sanitisedTitles = ['Mr' => 'Mister', 'Dr' => 'Doctor'];

    public function __construct(string $csvName)
    {
        $this->csvName = $csvName;
    }

    public function readCsv(): array
    {
        $resource = fopen($this->csvName . '.csv', "r");

        if ($resource) {
            while (($data = fgetcsv($resource, 1000, ",")) !== false) {
                $this->csvData[] = trim($data[0]);
            }

            fclose($resource);

            foreach ($this->csvData as $field) {
                $this->people[] = explode(" ", $field);
            }

            $this->checkForMultipleHomeowners();
        }

        return $this->people;
    }

    public function formatData(): array
    {
        $formattedPeople = [];

        foreach ($this->people as $key => $person) {
            $homeowner = new Homeowner();
            $homeowner->setTitle($this->sanitiseTitleFormat($person));

            $homeowner->setFirstName(null);
            $homeowner->setInitial(null);
            $homeowner->setLastName(null);

            $numOfNameComponents = count($person);

            if ($numOfNameComponents === 2) {
                $homeowner->setLastName($person[1]);
            }

            if ($numOfNameComponents === 3) {
                if ($this->isFirstNameInitial($person)) {
                    $homeowner->setInitial($this->removeDotAfterInitial($person));
                    $homeowner->setLastName($person[2]);
                } else {
                    $homeowner->setFirstName($person[1]);
                    $homeowner->setLastName($person[2]);
                }
            }

            $formattedPeople[] = $homeowner;
        }

        return $formattedPeople;
    }

    private function checkForMultipleHomeowners(): array
    {
        foreach ($this->people as $key => $person) {
            foreach($this->fieldSplitIndicators as $fieldSplitIndicator) {
                if (in_array($fieldSplitIndicator, $person)) {
                    $splitOffset = array_search($fieldSplitIndicator, $person);

                    $this->people[] = $this->splitPersonIntoTwo($person, $splitOffset);

                    $this->sanitiseOriginalDataAfterSplit($person, $splitOffset, $key);
                }
            }
        }

        return $this->people;
    }

    private function splitPersonIntoTwo(array $person, int $offset): array
    {
        $newPerson = array_slice($person, $offset + 1);


        if ($person[0] === 'Dr' && count($newPerson) === 3) {
            array_splice($newPerson, 1, 1);
        }

        return $newPerson;
    }

    private function sanitiseOriginalDataAfterSplit(array $person, int $offset, int $key): array
    {
        if ($offset === 1) {
            $lastName = $person[count($person) - 1];

            return array_splice($this->people[$key], 1, count($person), $lastName);
        }

        return array_splice($this->people[$key], $offset);
    }

    private function sanitiseTitleFormat(array $person): string
    {
        if (in_array($person[0], $this->sanitisedTitles)) {
            return array_search($person[0], $this->sanitisedTitles);
        }

        return $person[0];
    }

    private function isFirstNameInitial(array $person): bool
    {
        if (strlen($person[1]) === 1
            || (strlen($person[1]) === 2 && substr($person[1], -1) === ".")) {

            return true;
        }

        return false;
    }

    private function removeDotAfterInitial(array $person): string
    {
        if (substr($person[1], -1) === ".") {
            return substr($person[1], 0, 1);
        }

        return $person[1];
    }
}

