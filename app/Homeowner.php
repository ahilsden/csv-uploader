<?php

namespace App;

class Homeowner
{
    private string $title;
    private ?string $firstName;
    private ?string $initial;
    private ?string $lastName;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName = null): void
    {
        $this->firstName = $firstName;
    }

    public function getInitial(): ?string
    {
        return $this->initial;
    }

    public function setInitial(string $initial = null): void
    {
        $this->initial = $initial;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName = null): void
    {
        $this->lastName = $lastName;
    }
}

