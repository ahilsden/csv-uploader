<?php

require_once "./vendor/autoload.php";

use App\CsvUploader;

$csvUploader = new CsvUploader('data/names');
$csvUploader->readCsv();

$formattedHomeowners = $csvUploader->formatData();
?>

<html lang="en-gb">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homeowners</title>
</head>
<body>
    <h1>Homeowners</h1>
    <?php
        foreach ($formattedHomeowners as $key => $homeowner) {
            echo '<p>';
                echo '<h4>'; echo 'Homeowner: ' . $key; echo '</h4>';
                echo 'Title: ' . $homeowner->getTitle(); echo '<br>';
                echo 'First name: ' . $homeowner->getFirstName(); echo '<br>';
                echo 'Initial: ' . $homeowner->getInitial(); echo '<br>';
                echo 'Last name: ' . $homeowner->getLastName(); echo '<br>';
            echo '</p>';
        }
    ?>
</body>
</html>

