<?php

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;
use App\CsvUploader;

class CsvUploaderTest extends TestCase
{
    public function testThatValidFileSystemIsSetUp(): void
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Mr John Smith,
                                Mrs Jane Smith,
                                Mister John Doe"
            ],
        ];

        $root = vfsStream::setup('root', null, $csvStructure);

        $this->assertTrue($root->hasChild('data/names.csv'));
    }

    public function testItShouldReadValidCsvFile()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Mr John Smith,
                                Mrs Jane Smith,
                                Mister John Doe"
            ],
        ];

        $result = $this->readCsv($csvStructure);

        $this->assertCount(3, $result);
    }

    public function testMultipleHomeownersShouldSplitIntoTwo()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Mr and Mrs Smith,
                                Mrs Jane Townsend and Mr Edward Dobbs"
            ],
        ];

        $result = $this->readCsv($csvStructure);

        $this->assertCount(4, $result);
    }

    public function testOriginalDataRetainsNameComponentsAfterSplit()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Mr and Mrs Smith,
                                Mrs Jane Townsend and Mr Edward Dobbs"
            ],
        ];

        $result = $this->readCsv($csvStructure);

        $this->assertEquals(['Mr', 'Smith'], $result[0]);
        $this->assertEquals(['Mrs', 'Jane', 'Townsend'], $result[1]);
        $this->assertEquals(['Mrs', 'Smith'], $result[2]);
        $this->assertEquals(['Mr', 'Edward', 'Dobbs'], $result[3]);
    }

    public function testNewPersonRetainsNameComponentsAfterSplitWhenPartnerIsDoctor()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Dr & Mrs Joe Brown,
                                Dr and Mr Jane Jones
                                Dr & Mrs Edwards"
            ],
        ];

        $result = $this->readCsv($csvStructure);

        $this->assertEquals(['Dr', 'Brown'], $result[0]);
        $this->assertEquals(['Dr', 'Jones'], $result[1]);
        $this->assertEquals(['Dr', 'Edwards'], $result[2]);
        $this->assertEquals(['Mrs', 'Brown'], $result[3]);
        $this->assertEquals(['Mr', 'Jones'], $result[4]);
        $this->assertEquals(['Mrs', 'Edwards'], $result[5]);
    }

    public function testTitlesAreFormattedAccordingToPrescribedArray()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Doctor & Mrs Joe Brown,
                                Mister and Mrs Jane Jones"
            ],
        ];

        $result = $this->readCsv($csvStructure, true);

        $this->assertEquals('Dr', $result[0]->getTitle());
        $this->assertEquals('Mr', $result[1]->getTitle());
    }

    public function testDotIsRemovedAfterAnInitial()
    {
        $csvStructure = [
            'data' => [
                'names.csv' => "Doctor H. Pearson"
            ],
        ];

        $result = $this->readCsv($csvStructure, true);

        $this->assertEquals('H', $result[0]->getInitial());
    }

    private function readCsv(array $csvStructure, bool $formatCsv = false): array
    {
        $root = vfsStream::setup('root', null, $csvStructure);

        $csvUploader = new CsvUploader($root->url() . '/data/names');

        if ($formatCsv) {
            $csvUploader->readCsv();

            return $csvUploader->formatData();
        }

        return $csvUploader->readCsv();
    }
}

